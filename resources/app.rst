Gravitational Wave Events iOS App
=================================

There is an unofficial `Gravitational Wave Events app`_ for iOS devices.
Install this app on your iPhone or iPad to get real-time alerts.

.. _`Gravitational Wave Events app`: https://apps.apple.com/us/app/gravitational-wave-events/id1441897107
